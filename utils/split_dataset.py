from sklearn.model_selection import train_test_split 
from utils import set_seed
import numpy as np

def train_val_test_split(targets,split):
    set_seed.set_seed()
    train_idx, valid_idx= train_test_split(np.arange(len(targets)), 
                                                test_size=split[1]+split[2], shuffle=True, stratify=targets)
    if len(split)==2:
        return [train_idx, valid_idx]
    valid_targets=[targets[idx] for idx in valid_idx]
    set_seed.set_seed()
    valid_idx, test_idx= train_test_split(np.arange(len(valid_targets)), 
                                                test_size=split[2]/(split[1]+split[2]), shuffle=True, stratify=valid_targets)

    return [train_idx, valid_idx, test_idx]