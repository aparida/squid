import random

from glob import glob

import numpy as np  # linear algebra
import torch
from scipy.ndimage import interpolation as interp
from scipy.ndimage import measurements as meas
from skimage.transform import resize

from scipy.ndimage import shift
import torch.nn.functional as F
from torchvision import transforms
###################################################
# NOTE: All transforms work for 2D image and mask #
###################################################  
class Resize(object):
    """
    Resize an input to desired input
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        assert len(output_size) == 3 or len(output_size) == 2
        self.output_size = output_size

    def __call__(self, sample):
        image=[]
        label=[]
        for img in sample['image']:
            image.append(resize(img,self.output_size,order=3, mode='edge'))

        if len(sample['mask'])>0:
            for img in sample['mask']:
                label.append(resize(img,self.output_size,order=0, mode='edge'))
        
        return {'image':image,'mask':label}

class RandomRotate(object):
    """ Rotate 
        Since the standard is to keep the anterior north and posterior south,
        orientation should not be altered, rather just augmented 
    """

    def __init__(self, range):
        assert isinstance(range, (int, tuple))
        assert len(range) == 2
        self.degrees = (
            np.linspace(range[0], range[1], (range[1] - range[0]) + 1)
            .astype(int)
            .tolist()
        )
    def __call__(self, sample):
        
        self.execute = random.choice(list([0, 1]))
        choice = random.choice(self.degrees) * self.execute
        if choice == 0:
            return sample
        else:
            image=[]
            label=[]
            for img in sample['image']:
                image.append(interp.rotate(img, choice, reshape=False,mode='nearest'))

            if len(sample['mask'])>0:
                for img in sample['mask']:
                    mask = interp.rotate(img,choice,reshape=False,order=0,mode='nearest')
                    assert len(np.unique(mask)) <= 2, "Mask is not binary!!"
                    label.append(mask)
            
            return {'image':image,'mask':label}

class RandomLRFlip(object):
    """ Random mirror flip of X axis L <--> R
    """
    def __init__(self):
        self.execute =0

    def __call__(self, sample):
        
        self.execute = random.choice(list([0, 1]))
        if self.execute == 0:
            return sample
        else:
            image=[]
            label=[]
            for img in sample['image']:
                image.append(np.fliplr(img).astype(np.float32))
            if len(sample['mask'])>0:
                for img in sample['mask']:
                    mask = np.fliplr(img).astype(np.uint8)
                    assert len(np.unique(mask)) <= 2, "Mask is not binary!!"
                    label.append(mask)
            #import pdb; pdb.set_trace()
            return {'image':image,'mask':label}

class RandomUDFlip(object):
    """ Random mirror flip of X axis U <--> D
    """

    def __init__(self):
       pass

    def __call__(self, sample):
        self.execute = random.choice(list([0, 1]))
        if self.execute == 0:
            return sample
        else:
            image=[]
            label=[]
            for img in sample['image']:
                image.append(np.flipud(img).astype(np.float32))
            if len(sample['mask'])>0:
                for img in sample['mask']:
                    mask = np.flipud(img).astype(np.uint8)
                    assert len(np.unique(mask)) <= 2, "Mask is not binary!!"
                    label.append(mask)
            #import pdb; pdb.set_trace()
            return {'image':image,'mask':label}

class RandomTranslate(object):
    def __init__(self, limit):
        self.limit=limit
    def __call__(self,sample):
        v_translate = np.random.randint(self.limit[0],self.limit[1])
        h_translate = np.random.randint(self.limit[0],self.limit[1])
        self.execute = random.choice(list([0, 1]))
        if self.execute ==0:
            return sample
        else:
            image=[]
            label=[]
            
            if len(sample['mask'])>0:
                for img in sample['mask']:
                    mask = shift(img,(h_translate,v_translate),order=0,mode='nearest')
                    assert len(np.unique(mask)) <= 2, "Mask is not binary!!"
                    label.append(mask)

            for img in sample['image']:
                image.append(shift(img,(h_translate,v_translate),mode='nearest'))
        return {'image':image,'mask':label}

class WindowIze(object):
    def __init__(self, specs):
        self.specs=specs

    def window_image(self, scan, window_spec):
        window_center= window_spec[0] 
        window_width= window_spec[1] 
        img_min = window_center - window_width // 2
        img_max = window_center + window_width // 2
        scan = np.clip(scan, img_min, img_max)
        scan-=img_min
        scan/=img_max
        return scan

    def __call__(self,sample):
        image=[]

        for img in sample['image']:
            norm_list=[]
            for spec in self.specs:
                norm_list.append(self.window_image(img, spec))
            
            image.append(np.array(norm_list))
        return {'image':image,'mask':sample['mask']}

class CropCoG(object):
    """center crop an MR/CT volume based on center of gravity (COG)
       NOTE: output_size is not output_crop size!!!
             it is desired
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        assert len(output_size) == 2
        self.output_size = output_size

    def pad_crop_cog_(self,sample,mask):
        #import pdb;pdb.set_trace()
        assert len(sample.shape) == 2
        x,y = self.output_size
        orig_x,orig_y = sample.shape
        
        cog_x, cog_y = np.round(meas.center_of_mass(sample))
        xs = int(cog_x - x / 2)
        ys = int(cog_y - y / 2)
        xe = int(cog_x + x / 2)
        ye = int(cog_y + y / 2)


        # check if we need to pad
        xs_p = int(0 - xs) if xs < 0 else 0
        xe_p = int(0 - (orig_x - xe)) if (orig_x - xe) < 0 else 0
        ys_p = int(0 - ys) if ys < 0 else 0
        ye_p = int(0 - (orig_y - ye)) if (orig_y - ye) < 0 else 0


        npad = ((xs_p, xe_p), (ys_p, ye_p) )
        sample = np.pad(
            sample, pad_width=npad, mode='edge'
        )
        mask = np.pad(
            mask, pad_width=npad, mode='edge'
        )
        # reset after padding
        if xs < 0:
            xs = 0
            xe = x
        if ys < 0:
            ys = 0
            ye = y

        sample = sample[xs:xe, ys:ye]
        mask = mask[xs:xe, ys:ye]
        new_x, new_y = sample.shape
        assert (
            new_x == x and new_y == y 
        ), f" new sample shape of {sample.shape} does not match target"
        return sample, mask
    
    def pad_crop_cog(self,sample):
        #import pdb;pdb.set_trace()
        assert len(sample.shape) == 2
        x,y = self.output_size
        orig_x,orig_y = sample.shape
        
        cog_x, cog_y = np.round(meas.center_of_mass(sample))
        xs = int(cog_x - x / 2)
        ys = int(cog_y - y / 2)
        xe = int(cog_x + x / 2)
        ye = int(cog_y + y / 2)


        # check if we need to pad
        xs_p = int(0 - xs) if xs < 0 else 0
        xe_p = int(0 - (orig_x - xe)) if (orig_x - xe) < 0 else 0
        ys_p = int(0 - ys) if ys < 0 else 0
        ye_p = int(0 - (orig_y - ye)) if (orig_y - ye) < 0 else 0


        npad = ((xs_p, xe_p), (ys_p, ye_p) )
        sample = np.pad(
            sample, pad_width=npad, mode='edge'
        )

        # reset after padding
        if xs < 0:
            xs = 0
            xe = x
        if ys < 0:
            ys = 0
            ye = y

        sample = sample[xs:xe, ys:ye]
        new_x, new_y = sample.shape
        assert (
            new_x == x and new_y == y 
        ), f" new sample shape of {sample.shape} does not match target"
        return sample

    def __call__(self, sample):
        image=[]
        label=[]
        
            
        if len(sample['mask'])>0:
            for i, img in enumerate(sample['image']):
                img, mask = self.pad_crop_cog_(img,sample['mask'][i])
                assert len(np.unique(mask)) <= 2, "Mask is not binary!!"
                label.append(mask)
                image.append(img)
                
        else:
            for img in sample['image']:
                image.append(self.pad_crop_cog(img))
        return {'image':image,'mask':label}    

####################################
# UnCleared
###################################        

class RandomHistoProfile(object):
    """
        Using a saved database containing various histogram profiles of
        MR scans, a random profile is selected then applied with a 50/50
        chance to an incoming sample.
    """

    def __init__(self, path_to_profiles):
        assert isinstance(path_to_profiles, (str))
        self.profile = glob(path_to_profiles, recursive=True)
        assert len(self.profile) > 0
        self.profile = np.load(random.choice(self.profile))["histoProfiling"]
        self.execute = random.choice(list([0, 1]))

    def __call__(self, sample):
        choice = self.profile * self.execute
        if choice == 0:
            return sample
        else:
            oldshape = sample.shape
            sample = sample.ravel()
            # get the set of unique pixel values and their
            # corresponding indices and counts
            _, bin_idx, s_counts = np.unique(
                sample, return_inverse=True, return_counts=True
            )
            (t_values, t_counts) = self.profile
            # take the cumsum of the counts and normalize by the number of
            # pixels to get the empirical cumulative distribution functions
            # for the source and template images (maps pixel value -> quantile)
            s_quantiles = np.cumsum(s_counts).astype(np.float64)
            s_quantiles /= s_quantiles[-1]
            t_quantiles = np.cumsum(t_counts).histoastype(np.float64)
            t_quantiles /= t_quantiles[-1]
            # interpolate linearly to find pixel values in the template image
            # that correspond most closely to the quantiles in source image
            interp_t_values = np.interp(s_quantiles, t_quantiles, t_values)
            sample = interp_t_values[bin_idx].reshape(oldshape)
            return sample


class RescaleVol(object):
    """
        rescale vol to target shape
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size

    def __call__(self, sample):
        assert "float" in str(sample.dtype)
        if isinstance(sample, np.ndarray):
            sample = torch.from_numpy(sample.copy())
        sample = sample[None,]
        sample = F.interpolate(
            sample, size=(self.output_size[1:]), mode="nearest"
        )
        return sample.detach().numpy()[0,]
