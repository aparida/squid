import torch
from torchvision.utils import make_grid

def summarize_images(imgs, outs,labels, writer, ep):
    
    outs = torch.softmax(outs,1)
    mix = torch.argmax(outs,dim=1)
    # if len(imgs)>=24:
    #     idx = 24
    # else:
    #     idx = len(imgs)
    idx = [8,16,24]
    stack1 = torch.cat((imgs[:idx[0],0],mix[:idx[0]].float(),labels[:idx[0]].float()))
    stack2 = torch.cat((imgs[idx[0]:idx[1],0],mix[idx[0]:idx[1]].float(),labels[idx[0]:idx[1]].float()))
    stack3 = torch.cat((imgs[idx[1]:,0],mix[idx[1]:].float(),labels[idx[1]:].float()))
    
    imstack1 = make_grid(stack1, nrow=8,normalize=False)
    imstack2 = make_grid(stack2, nrow=8,normalize=False)
    imstack3 = make_grid(stack3, nrow=4,normalize=False)
    writer.add_images('masks/1',imstack1[:,None,:,:], ep)
    writer.add_images('masks/2',imstack2[:,None,:,:], ep)
    writer.add_images('masks/3',imstack3[:,None,:,:], ep)
    
def write_loss(writer, label, loss, iter_):
    if isinstance(loss, dict):
        writer.add_scalars(label, loss, iter_)
    else:
        writer.add_scalar(label, loss, iter_)