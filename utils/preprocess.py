import sys
import nibabel as nib
import numpy as np
from tqdm import tqdm
from pathlib import Path
from scipy import ndimage
import pandas as pd
import os
import glob
import re
import pickle

def write_pickle(data,path):
    with open(path, 'wb') as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)
        
def get_weights(img,disease_label):
    if disease_label!=0:
        bg,fg=np.bincount(img.flatten().astype(np.int64))
    else:
        return 1, img.shape[0]*img.shape[1]
    return (bg/float(bg+fg), fg/float(bg+fg), bg, fg, fg+bg)

def sliceXtract(root, img_list,seg_list,classes, atlas=False, resize=True):
    weights=[0]*len(classes)
    pixel_count={}
    freq_c={}
    img_count=0
    for i in classes:
        pixel_count[i]=[]
        freq_c[i]=[]
    # applies dataStats and extracts slices
    # for skipping black slices
    EPSILON = 1e-2
    SIZE_RESTRICT = 10240  # files less than 10kb are removed
    out_path = Path(f"{str(root)}/Sliced/")
    if not Path.exists(out_path):
        os.makedirs(out_path)
    for i, img in enumerate(tqdm(img_list,desc='Files')):
        nii = nib.load(str(Path(img)))
        zooms=nii.header.get_zooms
        seg_array_orig=nib.load(seg_list[i]).get_fdata()
        nii_array_orig = nii.get_fdata()
        if len(nii_array_orig.shape) == 4:
            nii_array_orig = nii_array_orig[:, :, :, 0]

        #nii_array = (nii_array_orig - np.min(nii_array_orig)) / (
        #    np.max(nii_array_orig) - np.min(nii_array_orig)
        #)
        # here we exclude slices that are essentially just air (empty)
        
        if resize:
            nii_array_orig = ndimage.zoom(nii_array_orig, nii.header['pixdim'][1:4],order=3)

            seg_array_orig = ndimage.zoom(seg_array_orig, nii.header['pixdim'][1:4],order=0,mode='nearest')

            
        if atlas:
            img='ATLAS.nii.gz'
        else:
            img=img.split('/')[-1]
        seg_array= seg_array_orig.T
        for rr, img_slice in enumerate(tqdm( nii_array_orig.T, desc='Slice',leave=False) ):
            if nii_array_orig[:, :, rr].max() <= 0 + EPSILON:
                continue
            # print(rr)
            # print(img_slice.max())
            seg_img = nib.Nifti1Image(
                seg_array[rr,...].T[:, :, np.newaxis].astype("int8"),
                nii._affine,
                header=nii.header,
            )
            array_img  = nib.Nifti1Image(
                img_slice.T[:, :, np.newaxis].astype("float32"),
                nii._affine,
                header=nii.header,
            )
            if len(np.unique(seg_array[rr,...]))==2:
                disease_label=1
                freq_bg,freq_fg,pix_bg, pix_fg,pix_total=get_weights(seg_array[rr,...],disease_label)
                
                pixel_count[disease_label].append(pix_fg)
                pixel_count[0].append(pix_bg)
                freq_c[disease_label].append(freq_fg)
                freq_c[0].append(freq_bg)
                img_count+=pix_total

                nib.save(array_img, f"{out_path}/{rr+1}_{img}")
                nib.save(seg_img, f"{out_path}/seg_{rr+1}_{img}")
            
            elif len(np.unique(seg_array[rr,...]))==1 :
                pass
                '''freq_bg,pix_total=get_weights(seg_array[rr,...],0)
                img_count+=pix_total
                freq_c[0].append(freq_bg)
                pixel_count[0].append(pix_total)'''
            else:
                
                assert True==False
                
                
            #nib.save(array_img, f"{out_path}/{rr+1}_{img}")
            #nib.save(seg_img, f"{out_path}/seg_{rr+1}_{img}")
            #final check of file size, if too small then delete
            '''if os.stat(f"{out_path}/{rr+1}_{img}").st_size < SIZE_RESTRICT:
                os.remove(f"{out_path}/{rr+1}_{img}")
                os.remove(f"{out_path}/seg_{rr+1}_{img}")
                
                img_count-=pix_total
                freq_c[0]=freq_c[0][:-1]
                pixel_count[0]=pixel_count[0][:-1]'''
                
    for cl in classes:
        weights[cl]=np.median(freq_c[cl])/(np.sum(pixel_count[cl])/img_count)
    
    return weights


if __name__ == "__main__":
    dataset_path='/home/abhi/TUM_Bleed/'

    file_list=sorted(glob.glob(dataset_path+'**.nii.gz'))
    seg_list=sorted(glob.glob(dataset_path+'seg_*.nii.gz'))
    file_list=[file for file in file_list if 'seg' not in file]
    print(len(file_list),len(seg_list))
    if len(file_list)==len(seg_list):
        weights=sliceXtract(dataset_path, file_list, seg_list,[0,1])
        write_pickle(weights,'./weights.pkl')
        
    print(weights)
    #sliceXtract(dataset_path, ['/home/abhi/atlas_matching/atlas/ATLAS.nii.gz'],atlas=True)
