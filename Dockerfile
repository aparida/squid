FROM pytorch/pytorch:latest
RUN python -m pip install --upgrade pip
RUN pip install nibabel
COPY . /workspace
WORKDIR /workspace/
RUN mkdir data
CMD python eval.py
