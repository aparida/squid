#!/usr/bin/env python
# coding: utf-8

import os
os.environ["CUDA_VISIBLE_DEVICES"]="0"

import datetime
import pickle
import torch.nn as nn
from dataloader.prepare_data import load_dataset
import matplotlib.pyplot as plt
from torch.utils.tensorboard import SummaryWriter
from torch.autograd import Variable
from utils.writer import *
from utils.set_seed import set_seed
from tqdm import trange, tqdm

import torch.optim as optim
import numpy as np

from models.loss import CEDiceLoss
from models.network import Segmentor

set_seed()
now = datetime.datetime.now()
device = 'cuda' if torch.cuda.is_available() else 'cpu'

def load_pickle(path):
    with open(path, 'rb') as handle:
        pickle_list=pickle.load(handle)
    return pickle_list 

save_dir = f'./saved/{str(now.date())}/{now.hour}_{now.minute}/'
os.makedirs(save_dir,exist_ok=True)
model_name=f'TUM_CT'

path_to_dataset='/home/abhi/TUM_Bleed/Sliced/'
epochs=500
lr=1e-3
batch=20 
split=(0.6,0.2,0.2)
imsize=(256,256)
shuffle=True 
transform=True

writer = SummaryWriter(f'./logs/{str(now.date())}/{model_name}_LR_{lr}_BATCH_{batch}')
dataset=load_dataset(path_to_dataset=path_to_dataset, batch=batch, split=split, imsize=imsize, shuffle=shuffle,  transform=transform)
train_ds=dataset[0]
val_ds=dataset[1]
test_ds=dataset[2]

print(f'DATASET LOADED:')
print(f'Train Samples:{len(train_ds)*batch}')
print(f'Valid Samples:{len(val_ds)*batch}')
print(f' Test Samples:{len(test_ds)*batch}')

model = Segmentor(2,in_channels=4).to(device)
#model = torch.load('/home/abhi/mri-brain/tum-brain-segmenter/saved/2020-02-20/12_56/TUM_CT_20_40.pth').to(device)
loss_fn = CEDiceLoss(device,ce_weights=[1,6])

learned_params = filter(lambda p: p.requires_grad, model.parameters())
optimizer = optim.Adam(learned_params, lr=lr, betas=(0.9, 0.999), eps=1e-08, weight_decay=0)
scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min',verbose=True, patience=5)

def get_loss(model, loss_fn, data,device, optimizer,train=True) :
    img, label= data
    img=Variable(img.float()).to(device)
    label=Variable(label.long()).to(device)
    
    optimizer.zero_grad()
    pred = model(img)
    dicemask=[1]*len(img)
    loss = loss_fn(pred, label, torch.FloatTensor(dicemask).to(device))
    #loss = loss_fn(pred, label)
    
    # train only
    if train:
        loss.backward()
        optimizer.step()

    return loss.item(),pred


for epoch in trange(epochs ,desc='Epochs', leave=False):
    # Train
    loss_ep=[]
    for i, data in enumerate(tqdm(train_ds,desc='Iters Trg',leave=False)):
        loss, pred= get_loss(model,loss_fn, data,device, optimizer,train=True)            
        loss_ep.append(loss)
        write_loss(writer, 'Itr Loss/train', loss, epoch*len(train_ds)+i)
        

    # Validation
    loss_val=[]
    for i, data in enumerate(tqdm(val_ds,desc='Iters Val',leave=False)):
        loss, pred= get_loss(model,loss_fn, data, device, optimizer,train=False)            
        loss_val.append(loss)
        write_loss(writer, 'Itr Loss/val', loss, epoch*len(val_ds)+i)
        
    # Images for summary writer
    img=Variable(data[0].float()).to(device)
    label=Variable(data[1].long()).to(device)
    summarize_images(img,pred,label,writer,epoch)
    
    write_loss(writer, 'Eps Loss/', {
                                    'train':  sum(loss_ep)/len(loss_ep),
                                    'val': sum(loss_val)/len(loss_val),
                                    }, epoch)
    
    scheduler.step(sum(loss_val)/len(loss_val))
    
    if (epoch+1)%10 ==0:
        print( f'{save_dir}{model_name}_{batch}_{epoch+1}.pth')
        torch.save(model, f'{save_dir}{model_name}_{batch}_{epoch+1}.pth')
          

