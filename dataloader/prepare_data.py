import torch
from torch.utils.data import DataLoader, Subset

from torchvision import transforms

from utils import set_seed
from utils.split_dataset import train_val_test_split

from dataloader.dataset import TUM_CT_BLEED
from utils.transform import *

set_seed.set_seed()
def load_dataset(path_to_dataset='/home/data/TUM_Bleed/Sliced/', batch=1, split=None, imsize=(256,256), shuffle=True,  transform=True):
    if transform:
         transformer = transforms.Compose([
                CropCoG((256,256)),
                Resize(imsize),
                RandomRotate((-10,10)),
                RandomTranslate((-5,5)),
                RandomLRFlip(),
                RandomUDFlip(),
                WindowIze([(40,80),(80,200),(40,380),(65,50)]) 
               # transforms.ToTensor()
             ])
    else:
        transformer = None

    dataset = TUM_CT_BLEED(path_to_dataset,transformer)

    if split is not None:
        assert len(split)>1 and len(split)<=3 and sum(split)==1
        split_idx= train_val_test_split(dataset.classes, split=split)
        
        data_loader_list=[]
        for split_id in split_idx:
            set_seed.set_seed()
            data_loader_list.append(DataLoader(Subset(dataset,split_id), batch_size=batch, shuffle=shuffle, drop_last=True))

        return data_loader_list

    set_seed.set_seed()
    return DataLoader(dataset, batch_size=batch, shuffle=shuffle)
 
