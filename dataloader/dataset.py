from torch.utils.data import Dataset
import glob
import nibabel as nib
import numpy as np
#from skimage import exposure
#from skimage.exposure import match_histograms


class TUM_CT_BLEED(Dataset):

    def __init__(self,path_data,transform):

        img_path=glob.glob(path_data+'*.nii.gz')
        self.train_path=sorted([path for path in img_path if 'seg' not in path])
        self.seg_path=sorted([path for path in img_path if 'seg' in path] )
        self.classes=[len(np.unique(nib.load(path).get_fdata()))-1 for path in self.seg_path ]
         
        self.transform = transform

    def __len__(self):
        return len(self.train_path)

    def load_image(self,path):
        return nib.load(path).get_fdata().squeeze()

    def load_seg(self, path):
        segName='/seg_'+ path.split('/')[-1]
        seg_path=[pth for pth in self.seg_path if segName in pth][0]
        return self.load_image(seg_path)
    
    def do_transform(self,img_list=[],mask_list=[]):
        image = self.transform({'image':img_list,
            'mask':mask_list})

        img_list =image['image']
        mask_list =image['mask']
        return img_list, mask_list

    def histo_match(self,img,atlas_img):
        img = match_histograms(img, atlas_img, multichannel=True)
        return img

    def __getitem__(self,idx):
        scan_path=self.train_path[idx]
        
        img=self.load_image(scan_path)
        seg_img=self.load_seg(scan_path)
        #img=self.histo_match(img,atlas_img)
        
        if self.transform:
            img_list, seg_list= self.do_transform(img_list=[img],mask_list=[seg_img])
            return img_list[0].astype(np.float16), seg_list[0].astype(np.int8)

        return img.astype(np.float8), seg_img.astype(np.float8)

              
