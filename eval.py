import os
os.environ["CUDA_VISIBLE_DEVICES"]=""
from models.network import Segmentor
from os.path import abspath
from os.path import dirname as d
import torch
import torch.optim as optim
import numpy as np

from models.loss import CEDiceLoss
from torch.autograd import Variable
import random
import csv
import glob
from pathlib import Path
import nibabel as nib

def do_infer():
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    
    lr=1e-3
    batch=20
    epochs=100//batch
    split=(0.6,0.2,0.2)
    imsize=(256,256)
    shuffle=True 
    transform=True


    model = Segmentor(2,in_channels=4).to(device)
    #model = torch.load('/home/abhi/mri-brain/tum-brain-segmenter/saved/2020-02-20/12_56/TUM_CT_20_40.pth').to(device)
    loss_fn = CEDiceLoss(device,ce_weights=[1,6])

    learned_params = filter(lambda p: p.requires_grad, model.parameters())
    optimizer = optim.Adam(learned_params, lr=lr, betas=(0.9, 0.999), eps=1e-08, weight_decay=0)
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min',verbose=True, patience=5)

    for epoch in range(epochs):
        img=torch.Tensor(np.zeros((batch,4, 256,256)))
        label=torch.Tensor(np.zeros((batch, 256,256)))

        img=Variable(img.float()).to(device)
        label=Variable(label.long()).to(device)
        optimizer.zero_grad()
        pred = model(img)
        dicemask=[1]*len(img)
        loss = loss_fn(pred, label, torch.FloatTensor(dicemask).to(device))

        loss.backward()
        optimizer.step()

def create_mask(path, filepath):
    filename=filepath.split('/')[-1]
    input_nii=nib.load(filepath)
    hdr=input_nii.header
    aff=input_nii.affine
    shape=input_nii.get_fdata().shape
    mask=input_nii.get_fdata()
    mask[mask<65]=0
    mask[mask>75]=0
    mask-=mask.min()
    mask/=mask.max()
    img = nib.Nifti1Image(mask, aff,hdr)
    nib.save(img, os.path.join(path,'seg_'+filename))

def create_csv(path, n=3):
    
    #Generate 5 random numbers between 10 and 30
    random_prob = random.sample(list(np.linspace(0, 1, 20,  dtype='float16')), n)
    

    with open(os.path.join(path,'pred_prob.csv'), 'w') as f:
        writer = csv.writer(f)
        writer.writerow(random_prob)
    with open(os.path.join(path,'pred_label.csv'), 'w') as f:
        writer = csv.writer(f)
        writer.writerow([np.argmax(random_prob)])

if __name__ == '__main__':
    do_infer()
    data_path='./data/'
    files=glob.glob(data_path+'/*/*.nii.gz')
    for file in files:
        path=Path(file).parent
        
        create_csv(path, n=3)
        create_mask(path,file)